
namespace Hello {

class About : Gtk.Dialog {

	public About() {
		title = "About";
		set_default_size(350, 100);

		Gtk.Label label = new Gtk.Label("Hello, Vala");

		Gtk.Box content = get_content_area () as Gtk.Box;
		content.pack_start(label, true, true, 0);
	}
}


}
