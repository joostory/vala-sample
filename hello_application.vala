namespace Hello
{

public class Application : Gtk.Application {
	public Application() {
		Object(application_id: "net.joostory.hello",
	       flags: ApplicationFlags.FLAGS_NONE);
	}

	protected override void startup() {
		base.startup();
		set_app_menu(make_menu());
		add_app_menu_actions();
		print ("startup\n");
	}

	protected override void shutdown() {
		print ("shutdown\n");
		base.shutdown();
	}

	protected override void activate() {
		base.activate();
		print ("activate\n");

		var window = new Window(this);
		window.show_all();
	}


	private GLib.Menu make_menu() {
		var menu = new GLib.Menu();
		menu.append("About", "app.about");
		menu.append("Menu1", "menu1");
		menu.append("Menu2", "menu2");
		menu.append("Menu3", "menu3");

		var section = new GLib.Menu();
		section.append("Section Menu1", "section.menu1");
		section.append("Section Menu2", "section.menu2");
		section.append("Section Menu3", "section.menu3");
		menu.append_section("Section", section);

		menu.append("Quit", "app.quit");
		return menu;
	}

	private void add_app_menu_actions() {
		var quit_action = new SimpleAction("quit", null);
		quit_action.activate.connect(this.quit);
		add_action(quit_action);

		var about_action = new SimpleAction("about", null);
		about_action.activate.connect(this.open_about);
		add_action(about_action);
	}

	private void open_about() {
		print ("Open About\n");
		var about = new About();
		about.show_all();
	}
}

}
