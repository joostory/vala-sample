
namespace Hello {

class Window : Gtk.ApplicationWindow {
	public Window(Application app) {
		set_application(app);
		set_default_size(400, 400);
		window_position = Gtk.WindowPosition.CENTER;
		title = "Hello";

		Gtk.Label label = new Gtk.Label ("Hello, Vala");
		add(label);
	}
}

}
