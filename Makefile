COMPILER=valac
FLAG=\
	--pkg gtk+-3.0
SOURCES=					\
	hello.vala				\
	hello_application.vala	\
	hello_window.vala		\
	hello_about.vala
EXEC=hello

all:
	@$(COMPILER) $(FLAG) $(SOURCES) -o $(EXEC)

clean:
	rm -f $(EXEC)
