
namespace Hello {

    public static void main(string[] args) {
        var app = new Application();
        app.run(args);
    }

}
